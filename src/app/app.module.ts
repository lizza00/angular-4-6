import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyComponent} from './my/my.component';
import { CarComponent } from './car/car.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
MyComponent,
CarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
