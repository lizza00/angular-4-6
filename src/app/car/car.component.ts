import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {

  // name = 'BMW';
  // price = 4645;
  // complecity = 'full';
  // discounts = [
  //   '22%',
  //   '10%',
  //   '3%'
  // ];
  // creditInfo = {
  //   parts: 12,
  //   sum: 300,
  //   name: 'super credit'
  // };
  //
  images = ['../../assets/img/1.jpeg',
    '../../assets/img/2.jpeg',
    '../../assets/img/3.jpeg'];
  carImageIndex = 0;
  carImage: string;
  showInfo = false;
  @Input() car;
  @Output() dataChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.carImage = this.images[this.carImageIndex];
    setInterval(() => {
      this.changeImage(true);
    }, 10000);
    // setTimeout(() => {
    //   this.price = '';
    //   this.complecity = '';
    // }, 2000);
  }

  changeImage(forward: boolean) {
    if (forward) {
      this.carImageIndex++;
    } else {
      this.carImageIndex--;
    }
    this.carImage = this.images[this.carImageIndex % this.images.length];
  }

  isExpensive() {
    return (+this.car.price) > 1000;
  }

  onChange() {
    this.dataChange.emit(new Date());
  }

  // changeName(name: string) {
  //   this.name = name;
  // }
}
