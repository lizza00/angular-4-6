import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-l4';
  changeDate = new Date();
  cars = [
    {
      name: 'Mersedes',
      price: 1213,
      complecity: `full`
    },
    {
      name: 'Subaru',
      price: 213,
      complecity: `half`
    }
  ];

  onDataChange(event) {
    this.changeDate = event;
  }
}
